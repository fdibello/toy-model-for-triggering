----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/11/2021 11:18:13 AM
-- Design Name: 
-- Module Name: testbench - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.math_real.uniform;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity testbench is
--  Port ( );
-- port ( A,B,C : in std_logic;
--         F    : out std_logic);
end testbench;

architecture Behavioral of testbench is

signal tb_clock  : std_logic := '0';
signal tb_clock2 : std_logic := '0';
signal tb_A : std_logic := '0';
signal tb_B : std_logic := '0';
signal tb_C : std_logic := '0';
signal tb_F : std_logic := '0';
signal random_eta_hits_l1 : std_logic_vector (9 downto 0);
signal random_eta_hits_l2 : std_logic_vector (9 downto 0);
signal random_eta_hits_l3 : std_logic_vector (9 downto 0);
signal tb_trig_decision   : std_logic_vector (9 downto 0);
signal tb_Fh : std_logic := '0';

begin

process
    begin
    tb_clock <= '1';
    tb_clock2 <= '1';
    tb_A <= '1';
    tb_B <= '1';
    tb_C <= '1';
    wait for 12.5 ns;
    tb_clock <= '0';
    tb_clock2 <= '0';
    tb_A <= '0';
    tb_B <= '0';
    tb_C <= '0';
    wait for 12.5 ns;
end process;



eta_hit_gen : for i in 0 to 9 generate
        eta_hit_logic : process
            -- this is the first seed for layer 1 --
            variable seed1_1 : integer := 144396720 + i;
            variable seed2_1 : integer := 121616997 + i;
            -- this is the first seed for layer 1 --
            variable seed1_2 : integer := 252394720 + i;
            variable seed2_2 : integer := 221446997 + i;
            -- this is the first seed for layer 1 --
            variable seed1_3 : integer := 399999999 + i;
            variable seed2_3 : integer := 301645565 + i;
            -- also generate hitting time --
            variable seed1_t : integer := 194999999 + i;
            variable seed2_t : integer := 331675065 + i;
            -- also generate hitting time --

            variable rnd_time : real;
            -- generate the variables used  to get the hit content --
            variable rnd_lay1 : real;
            variable rnd_lay2 : real;
            variable rnd_lay3 : real;
        begin
            -- initialize the hit to 0
            random_eta_hits_l1(i) <= '0';
            random_eta_hits_l2(i) <= '0';
            random_eta_hits_l3(i) <= '0';
            -- generate random numbers now
            uniform(seed1_1, seed2_1, rnd_lay1);
            uniform(seed1_2, seed2_2, rnd_lay2);
            uniform(seed1_3, seed2_3, rnd_lay3);
            uniform(seed1_t, seed2_t, rnd_time);
            --this is the genrating time of the dataset
            wait for rnd_time * 30 ns;
            --wait for rnd_time * 100 ns;
            if rnd_lay1 < 0.5 then
            --if rnd_time < 0.01 then --
                random_eta_hits_l1(i) <= '1';
            else
                random_eta_hits_l1(i) <= '0';
            end if;
            -- generate for the second layer now --
            if rnd_lay2 < 0.5 then
            --if rnd_time < 0.01 then --
                random_eta_hits_l2(i) <= '1';
            else
                random_eta_hits_l2(i) <= '0';
            end if;
            -- generate for the second layer now --
            if rnd_lay3 < 0.5 then
            --if rnd_time < 0.01 then --
                random_eta_hits_l3(i) <= '1';
            else
                random_eta_hits_l3(i) <= '0';
            end if;
            -- now let's wait a little bit further -- 
          
            wait for 10ns;
        end process;
    end generate;


-- parse this part of to the simulation to the actual architecture 


--tb_F <= NOT(A AND B AND C);

top_inst : entity work.top
  port map (
    clock   => tb_clock,
    A       => tb_A,
    B       => tb_B,
    C       => tb_C,
    F       => tb_F);
--    eta_hits => random_eta_hits,
--    F_h => tb_Fh );

top_inst2 : entity work.top_rnd
  port map (
    clock_h => tb_clock2,
    eta_hits_l1 => random_eta_hits_l1,
    eta_hits_l2 => random_eta_hits_l2,
    eta_hits_l3 => random_eta_hits_l3,
    trig_decision => tb_trig_decision,
    F_h => tb_Fh );




end Behavioral;
