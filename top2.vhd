----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/11/2021 11:15:14 AM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--entity top is
--    Port ( clock : in STD_LOGIC;
--           A,B,C : in std_logic;
--           eta_hits : in std_logic_vector(9 downto 0);
--           F_h  : out std_logic;
--           F    : out std_logic);
--end top;
--
--
---- clock generation --
--architecture Behavioral of top is
--
--begin
--
--F <= NOT(A AND B AND C);
--
--F_h <= eta_hits(0);
--
--end Behavioral;

-- define the input random data collector--
entity top_rnd is
    Port ( eta_hits_l1 : in std_logic_vector(9 downto 0);
           eta_hits_l2 : in std_logic_vector(9 downto 0);
           eta_hits_l3 : in std_logic_vector(9 downto 0);
           clock_h    : in std_logic;
           F_h    : out std_logic;
           trig_decision : out std_logic_vector(9 downto 0));
end top_rnd;


architecture Behavioral_sim of top_rnd is

begin



P_INCREMENT : process (clock_h)
  variable decision_lay3 : std_logic := '0';
  variable decision_lay1 : std_logic := '0';
begin
  if rising_edge(clock_h) then
    for i in 3 to 6 loop
        decision_lay3 :=  eta_hits_l3(i-2) OR eta_hits_l3(i-1) OR eta_hits_l3(i) OR eta_hits_l3(i+1) OR eta_hits_l3(i+2);
        decision_lay1 :=  eta_hits_l1(i-2) OR eta_hits_l1(i-1) OR eta_hits_l1(i) OR eta_hits_l1(i+1) OR eta_hits_l1(i+2);
        trig_decision(i) <= eta_hits_l2(i) AND decision_lay3 AND decision_lay1;
    end loop;
  trig_decision(0) <= '0';
  trig_decision(1) <= '0';
  trig_decision(2) <= '0';

  trig_decision(7) <= '0';
  trig_decision(8) <= '0';
  trig_decision(9) <= '0';
  end if;
end process;


---- for now I want to avoid coding edge effects and so I take a fiducial volume within the generated volume
--eta_hitsp : for i in 0 to 9 generate
--        eta_hit_trig : process
--        -- the second is the pivot and I do not need it
--        variable decision_lay3 : std_logic := '0';
--        variable decision_lay1 : std_logic := '0';
--
--        begin
---- need to check how do I pass this trig_decision to the entity? I shall inizialize it outside        
----        decision_lay3 :=  eta_hits_l3(i-2) OR eta_hits_l3(i-1) OR eta_hits_l3(i) OR eta_hits_l3(i+1) OR eta_hits_l3(i+2);
----        decision_lay1 :=  eta_hits_l1(i-2) OR eta_hits_l1(i-1) OR eta_hits_l1(i) OR eta_hits_l1(i+1) OR eta_hits_l1(i+2);
--
--        trig_decision(i) <= eta_hits_l2(i);
--        --trig_decision(i) <= eta_hits_l2(i) AND decision_lay3 AND decision_lay1;
--  
--
--        end process;
--    end generate;


  -- this is a test variable
  F_h <= eta_hits_l1(0) AND NOT(eta_hits_l2(0)) AND NOT(eta_hits_l3(0));



end Behavioral_sim;
