----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/11/2021 11:15:14 AM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
    Port ( clock : in STD_LOGIC;
           A,B,C : in std_logic;
--           eta_hits : in std_logic_vector(9 downto 0);
--           F_h  : out std_logic;
           F    : out std_logic);
end top;


-- clock generation --
architecture Behavioral of top is

begin

F <= NOT(A AND B AND C);

--F_h <= eta_hits(0);

end Behavioral;

-- define the input random data collector--
--library IEEE;
--use IEEE.STD_LOGIC_1164.ALL;
--entity top_rnd is
--    Port ( eta_hits : in std_logic_vector(9 downto 0);
--           F_h    : out std_logic);
--end top_rnd;
--
--
--architecture Behavioral_sim of top_rnd is
--
--begin
--
--F_h <= eta_hits(0);
--
--end Behavioral_sim;
